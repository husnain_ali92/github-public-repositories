import React from 'react';
import { css } from '@emotion/core';
import BeatLoader from 'react-spinners/BeatLoader';
import { Col } from 'reactstrap';
const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;
 
class Spinner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }
  render() {
    return (
        <Col md={12} sm={12} xs={12} className="text-center" style={{ marginTop : '20%' }}>
            <BeatLoader
                css={override}
                sizeUnit={"px"}
                size={20}
                color={'#17a2b8'}
                loading={this.props.showSpinner}
            />
        </Col>
    )
  }
}
export default Spinner;