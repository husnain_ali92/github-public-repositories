import React, { Component } from 'react';
import RepositoryList from './RepositoryListComponent';
import GithubSlider from './GithubSliderComponent';
import Header from './HeaderComponent';
import { Route, Switch } from "react-router-dom";

class Main extends Component {
    render() {
        return(
            <React.Fragment>
                <Header />
                <Switch>
                    <Route exact path="/" component={RepositoryList} />
                    <Route exact path="/githubslider" component={GithubSlider} />
                </Switch>
            </React.Fragment>
        )
    }
}
export default Main;
