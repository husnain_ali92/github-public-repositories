import React, { Component } from 'react';
import axios from 'axios';
import * as _ from 'lodash';
import Spinner from './SpinnerComponent';
import { Container, Row, Col, Button, Card, CardHeader, CardBody, CardTitle, CardText } from 'reactstrap';


class RepositoryList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            repositories : [],
            searchQuery : '',
            showSerachData : false,
            searchResult : [],
            showSpinner : true,
        }
    }
    
    componentDidMount() {
        setTimeout(() => {
            this.setState({ showSpinner : false })
        }, 1300);
        axios.get("https://api.github.com/repositories?since=0")
        .then((response) => {
            this.setState({ repositories : response.data });
        })
        .catch((error) => {
            console.log(error);
        })
    }
    
    render() {
        return (
            <Container className="mt-3">
                <Row>
                    <Col md={12} sm={12} className="text-center mb-3">
                        <span className="font-weight-bold">Github Public Repositories</span>
                    </Col>
                    <Col md={{ size : 6, offset : 3 }} sm={12} className="text-center mb-3">
                        <input type="search" value={this.state.searchQuery} className="form-control" placeholder="Search Repository" onChange={this.handleSearch} />
                    </Col>
                    {
                        this.state.showSpinner && <Spinner showSpinner={this.state.showSpinner} />
                    }
                    {
                        !this.state.showSpinner && !this.state.showSerachData && this.state.repositories.map((repository, index) => {
                            return(
                                <Col key={index} className="mt-2" md={4} sm={6} xs={12}>
                                    <Card outline color="info">
                                        <CardHeader className="text-uppercase font-weight-bold">{repository.full_name}</CardHeader>
                                        <CardBody>
                                            <CardTitle>Created By : <span className="text-uppercase">{repository.owner.login}</span></CardTitle>
                                            <CardText>Description : {_.truncate(repository.description, {'length': 40,'separator': ' '})}</CardText>
                                            <a href={repository.html_url} target="_blank" rel="noopener noreferrer"><Button color="info">Repository Link</Button></a>
                                        </CardBody>
                                    </Card>
                                </Col>
                            )
                        })
                    }
                    {
                        !this.state.showSpinner && this.state.showSerachData && this.state.searchResult.map((repository, index) => {
                            return(
                                <Col key={index} className="mt-2" md={4} sm={6} xs={12}>
                                    <Card outline color="info">
                                        <CardHeader className="text-uppercase font-weight-bold">{repository.full_name}</CardHeader>
                                        <CardBody>
                                            <CardTitle>Created By : <span className="text-uppercase">{repository.owner.login}</span></CardTitle>
                                            <CardText>Description : {repository.description}...</CardText>
                                            <a href={repository.html_url} target="_blank" rel="noopener noreferrer"><Button color="info">Repository Link</Button></a>
                                        </CardBody>
                                    </Card>
                                </Col>
                            )
                        })
                    }
                </Row>
            </Container>
        )
    }
    handleSearch = (event) => {
        this.setState({ showSerachData : true , searchQuery : event.target.value });
        event.target.value === '' ? this.setState({ showSerachData : false }) : this.setState({ showSerachData : true });
        this.filterData(event.target.value);
    }
    filterData = (searchQuery) => {
        let filteredArrayData = _.filter(this.state.repositories, (obj) => { return obj.name === searchQuery || obj.owner.login === searchQuery });
        this.setState({ searchResult : filteredArrayData });
        console.log("this.state.repositories...",this.state.searchResult);
        console.log("Filtered array Data",filteredArrayData);
    }
}
export default RepositoryList;
