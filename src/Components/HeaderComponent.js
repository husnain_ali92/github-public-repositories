import React, {Component} from 'react';
import {Navbar, NavbarBrand, Nav, NavbarToggler, Collapse, NavItem} from 'reactstrap';
import { NavLink } from 'react-router-dom';

class Header extends Component {
  constructor(props) {
      super(props);
      this.state = {
        isNavOpen: false,
      };
    }
    render() {
      return(
          <div>
              <Navbar color="light" light expand="md">
                  <div className="container">
                      <NavbarToggler onClick={this.toggleNav} />
                      <NavbarBrand className="mr-auto" href="/"><img src='https://cdn.worldvectorlogo.com/logos/react.svg' height="30" width="41" alt='Ristorante Con Fusion' /></NavbarBrand>
                      <Collapse isOpen={this.state.isNavOpen} navbar>
                          <Nav navbar>
                          <NavItem>
                              <NavLink className="nav-link"  to='/'><span className="fa fa-home fa-lg"></span>Home</NavLink>
                          </NavItem>
                          <NavItem>
                              <NavLink className="nav-link" to='/githubslider'><span className="fa fa-info fa-lg"></span>Slider</NavLink>
                          </NavItem>
                          </Nav>
                      </Collapse>
                  </div>
              </Navbar>
          </div>
      );
    }
    toggleNav = () => {
      this.setState({
        isNavOpen: !this.state.isNavOpen
      });
    }
}
export default Header;