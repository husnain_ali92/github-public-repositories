import React, { Component } from 'react';
import axios from 'axios';
import Spinner from './SpinnerComponent';
import { Container, Row, Col } from 'reactstrap';
class GithubSlider extends Component {
  constructor(props) {
      super(props);
      this.state = {
          repositories : [],
          showSpinner : true,
      }
  }
  
  componentDidMount() {
      setTimeout(() => {
          this.setState({ showSpinner : false })
      }, 1300);
      axios.get("https://api.github.com/repositories?since=0")
      .then((response) => {
          this.setState({ repositories : response.data });
      })
      .catch((error) => {
          console.log(error);
      })
  }
  
  render() {
      return (
        <React.Fragment>
          <Container className="mt-3">
              <Row>
                  <Col md={12} sm={12} className="text-center mb-3">
                      <span className="font-weight-bold">Github Public Repositories Carousel</span>
                  </Col>
                  {
                      this.state.showSpinner && <Spinner showSpinner={this.state.showSpinner} />
                  }
              </Row>
          </Container>
          {
            !this.state.showSpinner && 
            <div className="d-flex justify-content-center container mt-2">
                <ul className="d-inline-flex image-slider-wrapper">
                  {
                    this.state.repositories.map((repository, index) => {
                        return(
                          <li key={index} className="p-1 m-1">
                            <img src={repository.owner.avatar_url} alt="Avatar Not Available" className="sliderImg" />
                            <a href={repository.html_url} target="_blank" rel="noopener noreferrer"><button className="btn btn-info sliderCaption">See Repository</button></a>
                          </li>
                        )
                    })
                  }
                </ul>
            </div>
          }
        </React.Fragment>
      )
  }
  handleOnDragStart = e => e.preventDefault();
}
export default GithubSlider;