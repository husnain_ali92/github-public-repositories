import React from 'react';
import Main from './Components/MainComponent';

class App extends React.Component {
  render(){
    return (
      <Main />
    );
  }
}

export default App;
